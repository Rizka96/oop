<?php
    require_once ('animal.php');
    require_once ('ape.php');
    require_once ('frog.php');

    echo "<h4>Release 0</h4>";
    $sheep = new Animal("Shaun");
    echo "Binatang: " . $sheep->name . "<br>" ;// "shaun"
    echo "Jumlah Kaki: " . $sheep->legs . "<br>"; // 2
    echo $sheep->cold_blooded; // false
 
    echo "<h4>Release 1</h4>";
    $sungokong = new Ape("Kera Sakti");
    echo "Binatang: " . $sungokong->name . "<br>" ;
    echo "Jumlah Kaki: " . $sungokong->legs . "<br>"; 
    echo $sungokong->yell();// "Auooo"
    
    echo "<br><br>";
    $kodok = new Frog("Buduk");
    echo "Binatang: " . $kodok->name . "<br>" ;
    echo "Jumlah Kaki: " . $kodok->legs . "<br>"; 
    echo $kodok->jump(); // "hop hop"


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>